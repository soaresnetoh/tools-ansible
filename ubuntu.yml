---
- name: Preparing Workstation
  hosts: localhost
  connection: local
  
  tasks:
    - name: Installing Linux Apps
      become: true
      apt:
        name: '{{ item }}'
        install_recommends: yes
        state: present
      loop:
          - snapd
          - zsh
          - vim
          - htop
          - curl
          - wget
          - ncdu
          - tree
          - apt-transport-https
          - ca-certificates
          - gnupg
          - python3-pip
          - make
          - git
          - git-flow
          - bash-completion
          - gnupg-agent
          - flameshot
          - fonts-hack
          - tilix
          - mlocate
          - krita
          - jq
          - mtr
          - vinagre
          - rdesktop
          - xrdp
          - terminator
          - vlc
          - cmatrix
          - libarchive-tools
          - unzip
    - block:
      - name: VIRTUALBOX | add vb secure key
        become: true
        apt_key:
          url: "{{ item }}"
          state: present
        with_items:
          - "https://www.virtualbox.org/download/oracle_vbox_2016.asc"
          - "https://www.virtualbox.org/download/oracle_vbox.asc"

      - name: VIRTUALBOX | add paket sources
        become: true
        apt_repository:
          repo: "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian {{ ansible_distribution_release }} contrib"
          state: present

      - name: VIRTUALBOX | install virtualbox
        become: true
        apt:
          name: "{{ item }}"
          state: present
          update_cache: yes
        with_items:
          - "linux-headers-{{ ansible_kernel }}"
          - dkms
          - build-essential
          - "virtualbox-7.0"
    - block:
      - name: Verify if Oh-My-zsh is installed
        command: test -d /home/{{ ansible_user_id }}/.oh-my-zsh
        register: ohmyzsh
        ignore_errors: true
      - name: Installing Oh-My-zsh
        shell: >
          curl -o- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash
        when: ohmyzsh.rc != 0
        ignore_errors: true
      - name: Changing Default Shell to ZSH
        become: yes
        user: 
          name: '{{ ansible_user_id }}'
          shell: /bin/zsh 
      - name: Changing Default ZSH Theme to Agnoster
        lineinfile:
          path: /home/{{ ansible_user_id }}/.zshrc
          regexp: '^ZSH_THEME='
          line: 'ZSH_THEME="agnoster"'
      - name: Changing Default ZSH Theme to Bira
        lineinfile:
          path: /home/{{ ansible_user_id }}/.zshrc
          regexp: '^ZSH_THEME='
          line: 'ZSH_THEME="bira"'
      - name: Creating ZSH Completion folder
        file:
          path: /home/{{ ansible_user_id }}/.oh-my-zsh/completions
          state: directory
          mode: 0755
      - name: Adding autoload to compinit
        lineinfile:
          path: /home/{{ ansible_user_id }}/.zshrc
          line: 'autoload -U compinit && compinit'
          state: present
    - block:
      - name: Installing AWS CLI via pip3
        become: true
        pip:
          name: awscli
          executable: pip3
    - block:
      - name: Installing Vagrant 2.3.7
        become: true
        unarchive:
          src: 'https://releases.hashicorp.com/vagrant/2.3.7/vagrant_2.3.7_linux_amd64.zip'
          dest: /usr/local/bin
          remote_src: yes
    - block:
      - name: Installing Terraform 1.5.4
        become: true
        unarchive:
          src: 'https://releases.hashicorp.com/terraform/1.5.4/terraform_1.5.4_linux_amd64.zip'
          dest: /usr/local/bin
          remote_src: yes
    - block:
      - name: Install Microsoft Key
        become: true
        apt_key:
          url: 'https://packages.microsoft.com/keys/microsoft.asc'
          state: present
      - name: Install VSCode Repository
        become: true
        apt_repository:
          repo: 'deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main'
          state: present
          filename: vscode
      - name: Install Visual Studio Code
        become: true
        apt:
          name: code
      - name: Install Virtual Studio Code Extensions
        shell:
          cmd: code --install-extension '{{ item }}' 
        loop: 
          - ms-python.python
          - ms-azuretools.vscode-docker
          - bbenoist.vagrant
          - hashicorp.terraform
          - tfsec.tfsec
          - bbenoist.vagrant
          - gruntfuggly.todo-tree
          - njpwerner.autodocstring
          - eamodio.gitlens
          - golang.go
          - HashiCorp.HCL
          - bierner.markdown-preview-github-styles
          - redhat.vscode-yaml
          - MS-vsliveshare.vsliveshare
          - ms-azuretools.vscode-docker
          - github.copilot
          - ms-toolsai.jupyter
          - ms-toolsai.jupyter-keymap
          - ms-kubernetes-tools.vscode-kubernetes-tools
          - ms-vsliveshare.vsliveshare
          - shd101wyy.markdown-preview-enhanced
          - mongodb.mongodb-vscode
          - ms-vscode-remote.remote-containers
          - redhat.vscode-yaml
    - block: 
      - name: Install aptitude
        become: yes
        apt:
          name: aptitude
          state: latest
          update_cache: true

      - name: Install required system packages
        become: yes
        apt:
          pkg:
            - apt-transport-https
            - ca-certificates
            - curl
            - software-properties-common
            - python3-pip
            - virtualenv
            - python3-setuptools
          state: latest
          update_cache: true
         
      - name: Add Docker GPG apt Key
        become: yes
        apt_key:
          url: https://download.docker.com/linux/ubuntu/gpg
          state: present

      - name: Add Docker Repository
        become: yes
        apt_repository:
          repo: deb https://download.docker.com/linux/ubuntu focal stable
          state: present
          update_cache: yes

      - name: Update apt and install docker
        become: yes
        apt:
          update_cache: yes
          name: "{{ item }}"
          state: latest
        loop:
          - docker-ce 
          - docker-ce-cli 
          - containerd.io

      - name: Install Docker Module for Python
        pip:
          name: docker

      - name: Add user to the docker group
        user:
          name: "{{ item }}"
          groups: docker
          append: yes
        with_items: "{{ ansible_env.USER }}"
        become: yes
    - block:
      - name: Installing Kubectl 1.27.4 
        become: true
        get_url:    
          url: 'https://dl.k8s.io/release/v1.27.4/bin/linux/amd64/kubectl'
          dest: /usr/local/bin/kubectl
          mode: 755
    - block:
      - name: Adding Peek Repository
        become: true
        apt_repository:
          repo: 'ppa:peek-developers/stable'
      - name: Install Peek
        become: true
        apt:
          name: peek
    - block:
      - name: Install obsproject Ubuntu Repo
        become: true
        apt_repository:
          repo: 'ppa:obsproject/obs-studio'
      - name: Install obs Studio
        become: true
        apt:
          name: obs-studio
    - block: 
      - name: Install Brave Key
        become: true
        apt_key:
          url: 'https://brave-browser-apt-release.s3.brave.com/brave-core.asc'
          state: present
      - name: Install Brave Repository
        become: true
        apt_repository:
          repo: 'deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main'
          state: present
          filename: brave
      - name: Install Brave Browser
        become: true
        apt:
          name: brave-browser
    - block:
      - name: Adding Ulauncher Repository
        become: true
        apt_repository:
          repo: 'ppa:agornostal/ulauncher'
      - name: Install Ulauncher
        become: true
        apt:
          name: ulauncher
    - block:
      - name: Adding Telegram Repository
        become: true
        apt_repository:
          repo: 'ppa:atareao/telegram'
      - name: Install Telegram
        become: true
        apt:
          name: telegram
    - block:
      - name: Install "discord"
        command: snap install discord
        become: true
    - block:
      - name: Install "slack with option --classic"
        command: snap install slack --classic
        become: true
    - block:
      - name: Install "zoom-client"
        command: snap install zoom-client
        become: true
    - block:
      - name: Install "remmina"
        command: snap install remmina
        become: true
    - block:
      - name: Install "robo3t"
        command: snap install robo3t-snap
        become: true
    - block:
      - name: Install "TeamViewer"
        become: true
        apt:
          deb: https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
    - block:
      - name: Install nvm
        become_user: "{{ ansible_env.USER }}"
        git: repo=https://github.com/creationix/nvm.git dest=~/.nvm version=v0.39.4
        tags: nvm

      - name: Source nvm in ~/.{{ item }}
        become_user: "{{ ansible_env.USER }}"
        lineinfile: >
            dest=~/.{{ item }}
            line="source ~/.nvm/nvm.sh"
            create=yes
        tags: nvm
        with_items:
          - bashrc
          - profile
          - zshrc

      - name: Install node and set version
        become: yes
        become_user: "{{ ansible_env.USER }}"
        # shell: "source /root/.nvm/nvm.sh && nvm install 8.11.3" 
        shell: "source {{ ansible_env.HOME }}/.nvm/nvm.sh && nvm install {{item}} && npm install npm@latest -g"
        args:
          executable: /bin/bash
          chdir: "{{ ansible_env.HOME }}"
          creates: "{{ ansible_env.HOME }}/.nvm/versions/node/v{{item}}"
        loop:
          - "14.21.3"
